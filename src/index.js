import React from 'react';
import ReactDOM from 'react-dom';
import Main from "./Main";
import "./DataTables-1.10.20/datatables.min.css";
import './style.css';
import "./DataTables-1.10.20/jquery-3.4.1.min.js";
import "./DataTables-1.10.20/datatables.min.js";

ReactDOM.render(<Main />, document.getElementById('root'));